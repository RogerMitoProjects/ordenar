# Resumo

Este software foi desenvolvido utilizando-se a linguagem JAVA por meio da IDE NetBeans 7.2. Seu objetivo consiste em organizar em ordem alfabética diversas palavras que se encontram em arquivos texto por meio da aplicação de alguns dos mais famosos algoritmos de ordenação existentes. Ao todo são sete arquivos texto, cada um contento: 1000, 5000, 10000, 15000, 20000, 30000 ou 40000 palavras variadas e desorganizadas (fora de ordem alfabética). Ao final do processo de ordenação, um arquivo com as palavras organizadas em ordem alfabética é gerado na pasta de saída do mesmo. O tempo gasto por cada algoritmo também é apresentado ao usuário em sua tela principal. A classe principal do software possui algo em torno de 1500 linhas de código.

* Funcionamento:

A manipulação do programa é bem simples de ser executada; o usuário seleciona a quantidade de palavras a serem ordenadas e logo após escolhe  algoritmo que será utilizado na ordenação, bastando para isso clicar no botão com seu nome. Logo após isso o programa executará o processo utilizando o algoritmo escolhido, feito isso uma mensagem informando a conclusão da tarefa é exibida. O usuário ainda pode optar por visualizar o resultado da ordenação automaticamente, bastando para isso marcar a caixa “Exibir resultado”, onde será apresentada um tela com o conteúdo do arquivo ordenado. 


A tela principal do software pode ser conferida na figura abaixo:


![Getting Started](./01.png)

A figura a seguir destaca os principais componentes da tela inicial do software, juntamente com um breve resumo dos mesmos:

![Getting Started](./02.png)


Tela de confirmação de execução de teste:


![Getting Started](./03.png)

